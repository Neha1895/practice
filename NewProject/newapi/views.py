from rest_framework import viewsets
from django.shortcuts import render

# Create your views here.
from newapi.models import Person, Species
from newapi.serializers import PersonSerializer, SpeciesSerializer


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer


class SpeciesViewSet(viewsets.ModelViewSet):
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer
