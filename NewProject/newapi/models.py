from django.db import models


# Create your models here.
class Species(models.Model):
    name = models.CharField(max_length=50)
    classification = models.CharField(max_length=50)
    language = models.CharField(max_length=50)


class Person(models.Model):
    name = models.CharField(max_length=50)
    birth_year = models.CharField(max_length=50)
    eye_color = models.CharField(max_length=50)
    species = models.ForeignKey(Species, on_delete=models.DO_NOTHING)
