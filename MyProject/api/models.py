from django.db import models


# Create your models here.
class Student(models.Model):
    name = models.CharField(max_length=50)
    roll = models.IntegerField()
    city = models.CharField(max_length=100)


class Subject(models.Model):
    subid = models.IntegerField(primary_key=True)
    subname = models.CharField(max_length=100)
    totalpage = models.BigIntegerField()


class Book(models.Model):
    book_id = models.AutoField(primary_key=True)
    book_name = models.CharField(max_length=50, blank=True, null=True)
    book_auth = models.CharField(max_length=50, blank=True, null=True)
    subid = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=True, null=True, related_name="book")