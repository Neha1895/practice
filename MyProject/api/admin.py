from django.contrib import admin
from .models import Student, Subject, Book


# Register your models here.
@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "roll", "city"]


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ["subid", "subname", "totalpage"]


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ["book_id", "book_name", "book_auth", "subid"]
