def function1(*args):
   result = 0
   for i in args:
      result += i
   return result

print ("Output of first function:", function1(3,4,6))

print("**********************")

def function2(a, b, *args,  **kwargs):
   print("Output of second function:")
   print("a:", a,"b:" ,b)
   print("no. of argument:",args)
   print("Dict argument:",kwargs)

function2(1, 4, 6, 5, param1=5, param2=6)

print("**********************")


def function3(**kwargs):
  print("Her last name is " + kwargs["lname"])
  print("His first name is " +kwargs["fname"])
  print("His Father's name is " +kwargs["fathersname"])

function3(fname = "Smita", lname = "Patil",fathersname="Prakash")

print("**********************")


def function4(*arg,**kwargs):

   for key, value in kwargs.items():
        print("{0} = {1}".format(key, value))

   print("arg:",arg)
function4(100,200,CollageName="ModernCollage ")

print("**********************")

def function5(x,y,*arg,**kwargs):
  
    print("X:", x)
    print("Y:", y)
    print("Marks:", arg)
    for key, value in kwargs.items():
        print("{0} = {1}".format(key ,value))

function5("math","english",80,60,20,50,40,MyName="Amruta",LastNme="Patil")

print("**********************")


def function6(num1, num2, num3 ,num4, num5, num6):
    print("Output of function6:")
    print("num1:", num1)
    print("num2:", num2)
    print("num3:", num3)
    print("num4:", num4)
    print("num5:", num5)
    print("num6:", num6)

my_list = [10,20, 30,40,50]
function6(1, *my_list)

print("**********************")

def function7(*args):
 
    for x in args:
        print (x)
     
l = [110,3000,45.90,8.9,"I","am", "busy"]
 
print ("Ouputt of Function7:",function7(l))

print("**********************")

def function8(**kwargs): 
   
    result = []
    for key, value in kwargs.items():
        result.append("The capital city of {} is {} .format(key,value")
 
    return result
function8(India = "Delhi",Maharashtra = "Mumbai",Goa = "Panaji")

print("Output of function8:",function8)

print("**********************")

def function9(**kwargs):
    for key, value in kwargs.items():
         x = "{0} : {1}".format(key, value)
         print(x)

dict1 = {"a" : 100, "b" : 200, "c" : 300}

print("Dictionary:",function9(**dict1))

print("**********************")

def function10(*args):
    result = 0
    for x in args:
        result += x
    return result

list1 = [10, 20, 30]
list2 = [40, 50]
list3 = [60, 70, 80 ,90]

print("Sum of list elements :", function10(*list1, *list2, *list3))

print("**********************")