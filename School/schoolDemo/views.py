from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from rest_framework.decorators import api_view
from .models import Class_room,Student,Teacher
from django.views.decorators.csrf import csrf_exempt
from  schoolDemo.serializers import Class_room_serializer, Student_serilaizer,Teacher_serializer
from rest_framework.response import Response
import json
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 


# Create your views here.
@api_view(["GET"])    
def get_classRoom(request):
    print(request.method)
    abc = Class_room.objects.all()
    seri = Class_room_serializer(abc, many=True)
    return Response(seri.data)

@api_view(["POST"])   
def create_classRoom(request):
    data = request.data
    seri = Class_room_serializer(data=data)
    if seri.is_valid():
        class_obj = seri.save()
        return Response("classRoom added succesfully")
    return Response(seri.errors)

@csrf_exempt     
def delete_classroom(request,pk):
     if request.method == "DELETE":                
         cr = Class_room.objects.get(class_room_id=pk).delete()
         return HttpResponse("ClassRoom deleted succesfully")

@api_view(["PUT","PATCH"])  
def edit_classroom(request,pk):
  if request.method == 'PUT': 
      cr = Class_room.objects.get(class_room_id =pk)
      seri = Class_room_serializer(instance=cr,data=request.data)

      if seri.is_valid():
          seri.save()

      return Response("ClassRoom data updated succesfully")


           
@api_view(["POST"])   
def create_student_new(request):
    data = request.data
    seri = Student_serilaizer(data=data)
    if seri.is_valid():
        std_obj = seri.save()
        return Response("Student added succesfully")
    return Response(seri.errors)


@api_view(["GET"])  
def get_student(request):
    print(request.method)
    pqr = Student.objects.all()
    seri = Student_serilaizer(pqr, many=True)
    return Response(seri.data)

@api_view(["PUT","DELETE"])  #right
def deleteEdit_student(request,pk):
  if request.method == "DELETE":     
         stu =Student.objects.get(student_role_id=pk).delete()
         return HttpResponse({"student deleted succesfully"})
         

#def edit_student(request,pk):
  if request.method == 'PUT': 
      stu = Student.objects.get(student_role_id =pk)
      seri = Student_serilaizer(instance=stu,data=request.data)

      if seri.is_valid():
          seri.save()

      return Response("Student data updated succesfully")
       



@api_view(["GET","POST","DELETE","PUT"])
def teacher_details(request):
    if request.method == "GET":
    
        xyz = Teacher.objects.all()
        seri = Teacher_serializer(xyz, many=True)
        return Response(seri.data)


    if request.method =="POST":
        data = request.data
        seri = Teacher_serializer(data=data)
        if seri.is_valid():
             std_obj = seri.save()
             return Response("Teacher added succesfully")
        return Response(seri.errors)

@api_view(["PUT","PATCH"])  
def edit_teacher(request,pk):
    if request.method == "PUT":
        tea = Teacher.objects.get(teacher_role_id =pk)
        seri = Teacher_serializer(instance=tea,data=request.data)

        if seri.is_valid():
          seri.save()

        return Response("Teacher data updated succesfully")

@csrf_exempt       
def delete_teacher(request,pk):       
    if request.method == "DELETE":                
         tea = Teacher.objects.get(teacher_role_id=pk).delete()
         return HttpResponse("Teacher deleted succesfully")