"""School URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from schoolDemo.views import edit_classroom, edit_teacher, create_student_new,get_student,get_classRoom,delete_teacher,create_classRoom,delete_classroom,deleteEdit_student, teacher_details


urlpatterns = [
    path('admin/', admin.site.urls),
    path('getclass/',get_classRoom),
    path('newclass/',create_classRoom), 
    path('deleteclass/<str:pk>/',delete_classroom),
    path('editclass/<str:pk>/',edit_classroom), 
   
    path('newstudent/',create_student_new), 
    path('getstudent/',get_student),        
    path('student/<str:pk>/',deleteEdit_student),  
    #path('editstudent/<str:pk>/',edit_student), 
    
    path('teacher/',teacher_details),
    path('editteacher/<str:pk>/',edit_teacher),
    path('deleteteacher/<str:pk>/',delete_teacher)


    
    
]
