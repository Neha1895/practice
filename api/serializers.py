from rest_framework import serializers
from .models import Student,Subject,Book


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ["id", "name", "roll", "city"]

class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ["subid", "subname ", "totalpage"]

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        feilda = ["book_id", "book_name", "book_auth", "subid"]